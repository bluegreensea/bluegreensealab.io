'use strict';
var data;
$.getJSON('https://api.mcsrvstat.us/2/mc.fanlan.nctu.me',
    function(data){
    console.log(data);
    if(!data['online']){
        console.log('server is offline');
        $('#status').toggleClass('badge-light badge-danger');
        $('#status').text('離線')
        
    }
    else{
        console.log('server is online');
        $('#status').toggleClass('badge-light badge-success');
        $('#status').text('上線');
        $('#server').popover();
        var str = `<h5>
${data['motd']['html']}
<small class="badge badge-secondary">${data['version']}</small>
</h5>
<p>
玩家數: ${data['players']['online']} / ${data['players']['max']}
<p>`;
        $('#server').attr('data-content',str);

    }
    }
);

//console.log(data);